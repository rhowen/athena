// -*- C++ -*-
/**************************************************************************
 **
 **   File: Trigger/TrigHypothesis/TrigBPhysHypo/TrigL2DiMuXFex.cxx
 **
 **   Description: L2 hypothesis algorithms for B+ -> K+ mu+ mu-  
 **                Create TrigL2Bphys
 **
 **   Author: Cristina Adorisio (Cristina.Adorisio@cern.ch)
 **
 **   Created:   08.10.2007
 **   Modified:  25.02.2008(new TrigDiMuon class)
 **              07.03.2008(monitoring histograms added)
 **                
 **************************************************************************/ 
 
#include "TrigL2DiMuXFex.h"
#include "BtrigUtils.h"

#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"

#include "TrigTimeAlgs/TrigTimerSvc.h"

#include "TrigSteeringEvent/TrigRoiDescriptor.h"

// additions of xAOD objects
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTrigBphys/TrigBphys.h"
#include "TrigBphysHelperUtilsTool.h"

/*---------------------------------------------------------------------------------*/
TrigL2DiMuXFex::TrigL2DiMuXFex(const std::string & name, ISvcLocator* pSvcLocator):
HLT::FexAlgo(name, pSvcLocator),
m_bphysHelperTool("TrigBphysHelperUtilsTool"),
  m_L2vertFitter("TrigL2VertexFitter",this),
  m_vertexingTool("TrigVertexingTool",this)
{
  // Read cuts
  declareProperty("AcceptAll", m_acceptAll = false );  //default=true

  declareProperty("LowerMuMuMassCut", m_lowerMuMuMassCut = 200.0);  //default=200.0  
  declareProperty("UpperMuMuMassCut", m_upperMuMuMassCut = 5000.0);  //default=5000.0
  declareProperty("LowerKplusMuMuMassCut", m_lowerKplusMuMuMassCut = 5000.0);  //default=5000.0
  declareProperty("UpperKplusMuMuMassCut", m_upperKplusMuMuMassCut = 5600.0);  //default=5600.0

  declareProperty("doVertexFit", m_doVertexFit = true ); //default=true

   // variables for monitoring histograms
  declareMonitoredStdContainer("MuMuMass",      mon_dimumass  , AutoClear);
  declareMonitoredStdContainer("KplusMuMuMass", mon_kdimumass , AutoClear);
  
  //m_trigBphysColl = new TrigL2BphysContainer();
  
}

/*------------------------------*/
TrigL2DiMuXFex::~TrigL2DiMuXFex()
{ 
  delete m_trigBphysColl;
}

/*-------------------------------------------*/
HLT::ErrorCode TrigL2DiMuXFex::hltInitialize()
{
  msg() << MSG::INFO << "|----------------------- INFO FROM TrigL2DiMuXFex --------------------|" << endmsg;
  msg() << MSG::INFO << "Initializing TrigL2DiMuXFex" << endmsg;
  msg() << MSG::INFO << "AcceptAll            = " 
	<< (m_acceptAll==true ? "True" : "False") << endmsg; 
  msg() << MSG::INFO << "LowerMuMuMassCut     = " << m_lowerMuMuMassCut << endmsg;
  msg() << MSG::INFO << "UpperMuMuMassCut     = " << m_upperMuMuMassCut << endmsg;
  msg() << MSG::INFO << "LowerKplusMuMuMassCut    = " << m_lowerKplusMuMuMassCut << endmsg;
  msg() << MSG::INFO << "UpperKplusMuMuMassCut    = " << m_upperKplusMuMuMassCut << endmsg;
  msg() << MSG::INFO << "doVertexFit              = " 
	<< (m_doVertexFit==true ? "True" : "False") << endmsg; 
  msg() << MSG::INFO << "|---------------------------------------------------------------------|" << endmsg;
 
  m_lastEvent=0;
  m_lastEvent_DiMu=0;
  m_lastEventPassed=0;
  m_lastEventPassedMuMu=0;
  m_lastEventPassedKMuMu=0;

  m_countTotalEvents=0;
  m_countTotalEvents_DiMu=0;
  m_countTotalRoI=0;
  m_countTotalRoI_DiMu=0;

  m_countPassedEvents=0;
  m_countPassedRoIs=0;
  m_countPassedEventsMuMu=0;
  m_countPassedEventsKMuMu=0;

  m_countPassedMuMuMassCut=0;
  m_countPassedKMuMuMassCut=0;
  m_countPassedKMuMuVertexCut=0;

  if ( m_doVertexFit ){
    StatusCode sc = m_L2vertFitter.retrieve();
    if ( sc.isFailure() ) {
      msg() << MSG::FATAL << "Unable to locate TrigL2VertexFitter tool" << endmsg;
      return HLT::BAD_JOB_SETUP;
    } 
    else {
      msg() << MSG::INFO << "TrigL2VertexFitter retrieved"<< endmsg;
    }
    
    sc = m_vertexingTool.retrieve();
    if ( sc.isFailure() ) {
      msg() << MSG::FATAL << "Unable to locate TrigVertexingTool tool" << endmsg;
      return HLT::BAD_JOB_SETUP;
    } else {
      msg() << MSG::INFO << "TrigVertexingTool retrieved"<< endmsg;
    }
  }
    if (m_bphysHelperTool.retrieve().isFailure()) {
        msg() << MSG::ERROR << "Can't find TrigBphysHelperUtilsTool" << endmsg;
        return HLT::BAD_JOB_SETUP;
    } else {
        if (msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "TrigBphysHelperUtilsTool found" << endmsg;
    }

  // add timers
  if ( timerSvc() ) {
    m_TotTimer    = addTimer("L2DiMuXFex_Tot");
    m_VtxFitTimer = addTimer("L2DiMuXFex_KplusMuMuVFit");
  }  
  return HLT::OK;
}

/*---------------------------------------------*/
HLT::ErrorCode TrigL2DiMuXFex::hltFinalize()
{
  msg() << MSG::INFO << "Finalizing TrigL2DiMuXFex" << endmsg;
  
  msg() << MSG::INFO << "|----------------------- SUMMARY FROM TrigL2DiMuXFex -----------------|" << endmsg;
  msg() << MSG::INFO << "Run on events / RoIs " << m_countTotalEvents  << " / " << m_countTotalRoI <<  endmsg;
  msg() << MSG::INFO << "Run on events / RoIs " << m_countTotalEvents_DiMu  << " / " << m_countTotalRoI_DiMu << " from DiMuon algo" <<  endmsg;
  if ( !m_doVertexFit ) {
    msg() << MSG::INFO << "Passed events / RoIs " << m_countPassedEvents << " / " << m_countPassedRoIs <<  endmsg;
  } else {
    msg() << MSG::INFO << "Passed events / RoIs " << m_countPassedEventsKMuMu << " / " << m_countPassedKMuMuVertexCut <<  endmsg;
  }
  msg() << MSG::INFO << "Passed MuMu Invariant Mass Cut ( events / RoIs ) : "      << m_countPassedEventsMuMu  << " / " << m_countPassedMuMuMassCut << endmsg;
  msg() << MSG::INFO << "Passed KplusMuMu Invariant Mass Cut ( events / RoIs ) : " << m_countPassedEvents      << " / " << m_countPassedKMuMuMassCut << endmsg;
  if ( m_doVertexFit )
    msg() << MSG::INFO << "Passed KplusMuMu vertexing ( events / RoIs ) : "          << m_countPassedEventsKMuMu << " / " << m_countPassedKMuMuVertexCut << endmsg;
  msg() << MSG::INFO << "|---------------------------------------------------------------------|" << endmsg;

  return HLT::OK;
}

//----------------------------------------------------------------------------------------------------------
HLT::ErrorCode TrigL2DiMuXFex::hltExecute(const HLT::TriggerElement* /*inputTE*/, HLT::TriggerElement* outputTE)
{
  if ( timerSvc() ) 
    m_TotTimer->start();


  if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Executing TrigL2DiMuXFex" << endmsg;

  bool result      = false;
  bool resultMuMu  = false;
  bool resultKMuMu = false;

  bool PassedMuMuPair    = false;
  bool PassedKMuMuMass   = false;
  bool PassedKMuMuVertex = false;

    //  std::vector<const TrigInDetTrackFitPar*> particleOK;
    //  std::vector<const TrigInDetTrack*> v_particleOK;
    
    std::vector<const xAOD::TrackParticle*> v_particleOK;

    // Retrieve event info
    int IdEvent = 0;
    int IdRun   = 0;
    // event info
    uint32_t runNumber(0), evtNumber(0), lbBlock(0);
    if (m_bphysHelperTool->getRunEvtLb( runNumber, evtNumber, lbBlock).isFailure()) {
        msg() << MSG::ERROR << "Error retriving EventInfo" << endmsg;
    }
    IdEvent = evtNumber;
    IdRun   = runNumber;

    
    // Accept-All mode: temporary patch; should be done with force-accept
  if ( m_acceptAll ) {
    if ( msgLvl() <= MSG::DEBUG ){
      msg() << MSG::DEBUG << "AcceptAll property is set: taking all events" << endmsg;
    }
  } else {
    if ( msgLvl() <= MSG::DEBUG ){
      msg() << MSG::DEBUG << "AcceptAll property not set: applying selection" << endmsg;
    }
  }

  // get RoI descriptor
  const TrigRoiDescriptor* roiDescriptor = 0;
  if ( getFeature(outputTE, roiDescriptor) != HLT::OK )
    roiDescriptor = 0;
  if ( roiDescriptor ) {
    if ( msgLvl() <= MSG::DEBUG ){
      msg() << MSG::DEBUG 
	    << "Using outputTE ( " << outputTE << " ) ->getId(): " << outputTE->getId() << endmsg;
      msg() << MSG::DEBUG << "RoI id " << roiDescriptor->roiId()
	    << " LVL1 id " << roiDescriptor->l1Id()
	    << " located at   phi = " <<  roiDescriptor->phi()
	    << ", eta = " << roiDescriptor->eta() << endmsg;
    }
  } else {
    if ( msgLvl() <= MSG::WARNING ) {
      msg() << MSG::WARNING << "No RoI for this Trigger Element!" << endmsg;
    }
    if ( timerSvc() ) 
      m_TotTimer->stop();    
    return HLT::NAV_ERROR;
  }

  if ( IdEvent != m_lastEvent ) {
    m_countTotalEvents++;
    m_lastEvent=IdEvent;
  }
  m_countTotalRoI++;

  //  create vector for TrigL2Bphys particles and vertexes
    //  if ( !m_trigBphysColl )
    //    m_trigBphysColl = new TrigL2BphysContainer();
    //  else
    //    m_trigBphysColl->clear();

    m_trigBphysColl = new xAOD::TrigBphysContainer();
    xAOD::TrigBphysAuxContainer trigBphysAuxColl;
    m_trigBphysColl->setStore(&trigBphysAuxColl);
    
  // get vector for TrigL2Bphys particles
    const xAOD::TrigBphysContainer* trigBphysColl(nullptr);
  HLT::ErrorCode status = getFeature ( outputTE, trigBphysColl );
  if ( status != HLT::OK ) {
    if ( msgLvl() <= MSG::WARNING ) {
      msg() << MSG::WARNING << "Failed to get TrigBphysics collection" << endmsg;
    }
    return HLT::OK;
  } else {
    if ( trigBphysColl == NULL ) {
      if ( msgLvl() <= MSG::DEBUG ) {
	msg() << MSG::DEBUG << "No Bphys particles found" << endmsg;
      }
      return HLT::OK;
    }
    if ( msgLvl() <= MSG::DEBUG ) {
      msg() << MSG::DEBUG << "Got TrigBphys collection with " << trigBphysColl->size() << " Bphys particle candidates" << endmsg;
    }
    // to count events from TrigDiMuon
    if ( IdEvent != m_lastEvent_DiMu ) {
      m_countTotalEvents_DiMu++;
      m_lastEvent_DiMu=IdEvent;
    }
    m_countTotalRoI_DiMu++;
  }

  xAOD::TrigBphysContainer::const_iterator endPair = trigBphysColl->end();
  for ( xAOD::TrigBphysContainer::const_iterator thePair = trigBphysColl->begin(); thePair != endPair; ++thePair ){

    //if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "particleType = " << (*thePair)->particleType() << endmsg;
    if ( (*thePair)->particleType() == xAOD::TrigBphys::JPSIMUMU) {

      float mass = (*thePair)->mass();
      if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "MuMu Invariant Mass = " << mass << endmsg;

      // apply mass cut
      if ( mass < m_lowerMuMuMassCut || mass > m_upperMuMuMassCut) {
	if ( msgLvl()<= MSG::DEBUG ){
	  msg() << MSG::DEBUG << "Combination discarded by mass cut: "
		<< mass << " MeV" << endmsg;
	}
          //particleOK.clear(); //to have only the muon pair in the vector
          v_particleOK.clear();
          continue;
      } else {
          mon_dimumass.push_back((mass*0.001));
          PassedMuMuPair=true;
          resultMuMu = true;
          if( msgLvl() <= MSG::DEBUG ) {
              msg() << MSG::DEBUG << "MuMu combination retained! Invariant mass value: "
              << mass << " MeV" << endmsg;
          }
          //particleOK.clear(); //to have only the muon pair in the vector
	v_particleOK.clear(); 
      }

      // JK 22-05-08 changes for new EDM
      //      const TrigVertex* p_vtx = (*thePair)->pVertex();
      //      if ( p_vtx != NULL ) {
      // if vertex exists check tracks
      //	TrackInVertexList* p_tracks=p_vtx->tracks();
        //const ElementLinkVector<TrigInDetTrackCollection> trackVector = (*thePair)->trackVector();
        const std::vector<ElementLink<xAOD::TrackParticleContainer> > & trackVector = (*thePair)->trackParticleLinks();
      if ( msgLvl() <= MSG::VERBOSE) msg() << MSG::VERBOSE << "Number of tracks in vertex:  " << trackVector.size() << endmsg;
      
      //std::list<const TrigInDetTrack*>::iterator vtrkIt=p_tracks->begin();	
      //std::list<const TrigInDetTrack*>::iterator vtrkItEnd=p_tracks->end();	
      std::vector<ElementLink<xAOD::TrackParticleContainer> >::const_iterator vtrkIt   =trackVector.begin();
      std::vector<ElementLink<xAOD::TrackParticleContainer> >::const_iterator vtrkItEnd=trackVector.end();
        for ( int itrk=0; vtrkIt != vtrkItEnd; ++vtrkIt, ++itrk ) {
            //  JW OLD          const TrigInDetTrackFitPar* p_param=(*(*vtrkIt))->param();
            //            if ( p_param == NULL ) {
            //                if( msgLvl() <= MSG::WARNING ) {
            //                    msg() << MSG::WARNING << "No track parameters for track  " << itrk << endmsg;
            //                }
            //                continue;
            //            }
            //            if ( (*(*vtrkIt))->chi2() == 100000000.000 ) {
            //                if( msgLvl() <= MSG::WARNING ) msg() << MSG::WARNING << "Chi2 of muon track " << itrk << " is 1e8 !" << endmsg;
            //                continue;
            //            }
            //            if ( msgLvl() <= MSG::VERBOSE) msg() << MSG::VERBOSE << "Parameters for track " << itrk << " : pT = "
            //                << p_param->pT() << " phi = " << p_param->phi0() << " eta = "
            //                << p_param->eta() << endmsg;
            //            particleOK.push_back(p_param);	  
            //            v_particleOK.push_back(*(*vtrkIt));
            
            if ( (*(*vtrkIt))->chiSquared() == 100000000.000 ) {
                if( msgLvl() <= MSG::WARNING ) msg() << MSG::WARNING << "Chi2 of muon track " << itrk << " is 1e8 !" << endmsg;
                continue;
            } // if chi2
            if ( msgLvl() <= MSG::VERBOSE) msg() << MSG::VERBOSE << "Parameters for track " << itrk << " : pT = "
                << (*(*vtrkIt))->pt() << " phi = " << (*(*vtrkIt))->phi() << " eta = "
                << (*(*vtrkIt))->eta() << endmsg;
            //particleOK.push_back(p_param);
            //v_particleOK.push_back(*(*vtrkIt));
            v_particleOK.push_back(*(*vtrkIt));
        } // for trks
      
      //-----------------------Get vector of tracks ----------------------
        //std::vector<const TrigInDetTrackCollection*> vectorOfTrackCollections;
        std::vector<const xAOD::TrackParticleContainer*> vectorOfTrackCollections;
      status = getFeatures( outputTE, vectorOfTrackCollections );
      
      if ( trigBphysColl != NULL ){
	if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "TrigBphys collection with " << trigBphysColl->size() << " muon pairs candidates" << endmsg;
	
	if ( status != HLT::OK ) {
	  if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Failed to get InDetTrackCollections " << endmsg;
	} else {
	  if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Got " << vectorOfTrackCollections.size() << " InDetTrackCollections" << endmsg;
	}
	
	std::vector<const xAOD::TrackParticleContainer*>::iterator lastTrackColl = vectorOfTrackCollections.end();
	for ( std::vector<const xAOD::TrackParticleContainer*>::iterator iterTrackColl = vectorOfTrackCollections.begin(); iterTrackColl != lastTrackColl; ++iterTrackColl ){
	  
	  xAOD::TrackParticleContainer::const_iterator track= (*iterTrackColl)->begin();
	  xAOD::TrackParticleContainer::const_iterator lastTrack= (*iterTrackColl)->end();
	  if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Found " << (*iterTrackColl)->size() << " tracks" << endmsg;	 
	  if ( (*iterTrackColl)->size() <= 2 )
	    continue;
	  
	  for ( int itrk=0; track !=lastTrack; ++itrk, ++track ) {
	    
          unsigned int algoId = (*track)->trackProperties(); //JW (*track)->algorithmId();
	    if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Info on InDet tracks, track: " << itrk << ", algo " << algoId
		  << " pT = " << (*track)->pt() << " phi = " << (*track)->phi()
		  << " eta = " << (*track)->eta() << endmsg;

	    if ( (*track)->chiSquared() == 100000000.000 ) continue;
	    
          //particleOK.push_back((*track)->param());
	    v_particleOK.push_back((*track));        
	    std::vector<double> massHypo;
	    massHypo.clear();
	    massHypo.push_back(MUMASS);
	    massHypo.push_back(MUMASS);
	    massHypo.push_back(KPLUSMASS);
	    
	    if ( std::fabs(v_particleOK[2]->pt() - v_particleOK[0]->pt()) < EPSILON || std::fabs(v_particleOK[2]->pt() - v_particleOK[1]->pt()) < EPSILON ){
            //particleOK.pop_back();
	      v_particleOK.pop_back();
	      continue;
	    } 
	    if ( v_particleOK[2]->charge() < 0 ){   // only positive tracks (B+ case)
            //particleOK.pop_back();
	      v_particleOK.pop_back();
	      continue;
	    }
	    if ( std::fabs(v_particleOK[2]->pt() - v_particleOK[0]->pt()) > EPSILON && std::fabs(v_particleOK[2]->pt() - v_particleOK[1]->pt()) > EPSILON ){
            //double BmassHypo = InvMass(particleOK, massHypo);
            double BmassHypo = m_bphysHelperTool->invariantMass(v_particleOK,massHypo);
	      if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "3 particles inv mass = " << BmassHypo << endmsg;
            //particleOK.pop_back();
	      v_particleOK.pop_back();
	      
	      // apply mass cut
	      if ( BmassHypo < m_lowerKplusMuMuMassCut || BmassHypo > m_upperKplusMuMuMassCut ) {
		if ( msgLvl()<= MSG::DEBUG ){
		  msg() << MSG::DEBUG << "3 particles combination discarded by mass cut: "
			<< BmassHypo << " MeV" << endmsg;
		}
		continue;
	      } else {
		mon_kdimumass.push_back((BmassHypo*0.001));
		PassedKMuMuMass = true;
		result = true;
		if( msgLvl() <= MSG::DEBUG ) {
		  msg() << MSG::DEBUG << "3 particles combination retained! Invariant mass value: "
			<< BmassHypo << " MeV" << endmsg;
		}
	      }
	      
	      
            //store new TrigL2Bphys collection
            //TrigL2Bphys* trigPartBmumuX = new TrigL2Bphys(0, 0., 0., TrigL2Bphys::BMUMUX, BmassHypo);
            xAOD::TrigBphys* trigPartBmumuX = new xAOD::TrigBphys();
            trigPartBmumuX->makePrivateStore();
            trigPartBmumuX->initialise(0, 0., 0., xAOD::TrigBphys::BMUMUX, BmassHypo,xAOD::TrigBphys::L2);
            
            if( msgLvl() <= MSG::DEBUG ) {
                msg()  << MSG::DEBUG << "Create Bphys particle with roI Id " << trigPartBmumuX->roiId()
                << " phi = " << trigPartBmumuX->phi() << " eta = " << trigPartBmumuX->eta()
                << " mass = " << trigPartBmumuX->mass()
                << " particle type (trigPartBmumuX->particleType()) " << trigPartBmumuX->particleType() << endmsg;
            }
            // need to find index of tracks in collection
            xAOD::TrackParticleContainer::const_iterator it = (*iterTrackColl)->begin(),e = (*iterTrackColl)->end();
            int itr1=-99;
            int itr2=-99;
            int itr3=-99;
            for (int itrk=0; it != e; ++it,++itrk) {
                if (v_particleOK[0] == (*it)) itr1=itrk;
                if (v_particleOK[1] == (*it)) itr2=itrk;
                if (v_particleOK[2] == (*it)) itr3=itrk;
            }
            if (itr1==-99 || itr2==-99 || itr3==-99) msg() << MSG::INFO << "Something is worng, can't find tracks in original container" << endmsg;
            ElementLink<xAOD::TrackParticleContainer> track1EL(*(*iterTrackColl),itr1);
            ElementLink<xAOD::TrackParticleContainer> track2EL(*(*iterTrackColl),itr2);
            ElementLink<xAOD::TrackParticleContainer> track3EL(*(*iterTrackColl),itr3);
            
            trigPartBmumuX->addTrackParticleLink(track1EL);
            trigPartBmumuX->addTrackParticleLink(track2EL);
            trigPartBmumuX->addTrackParticleLink(track3EL);
            
            if ( m_doVertexFit ){
                
                if ( timerSvc() )
                    m_VtxFitTimer->start();
                
                // creating empty TrigL2Vertex
                TrigL2Vertex* vL2Kmumu = new TrigL2Vertex();
                
                TrigVertex* p_KmumuV = NULL;
                
                // adding TrigInDetTrack* to the vertex
                StatusCode sc_v = m_vertexingTool->addTrack(v_particleOK[0]->track(), vL2Kmumu, Trk::muon);
                if(sc_v.isFailure()){
                    if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Failed to add track 1 to vertexingTool" << endmsg;
                }
                sc_v = m_vertexingTool->addTrack(v_particleOK[1]->track(), vL2Kmumu, Trk::muon);
                if(sc_v.isFailure()){
                    if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Failed to add track 2 to vertexingTool" << endmsg;
                }
                sc_v = m_vertexingTool->addTrack(v_particleOK[2]->track(), vL2Kmumu, Trk::kaon);
                if(sc_v.isFailure()){
                    if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Failed to add track 3 to vertexingTool" << endmsg;
                }
                
                // vertex fit
                StatusCode sc_vert = m_L2vertFitter->fit(vL2Kmumu);
                
                if ( sc_vert.isSuccess() ) {
                    //invariant mass calculation
                    sc_vert = m_vertexingTool->calculateInvariantMass(vL2Kmumu);
                    if ( sc_vert.isSuccess() ) {
                        //mother particle creation
                        sc_vert = m_vertexingTool->createMotherParticle(vL2Kmumu);
                        if ( sc_vert.isSuccess() ) {
                            if ( msgLvl() <= MSG::DEBUG) {
                                msg() << MSG::DEBUG << "B+ created:" << endmsg;
                                msg() << MSG::DEBUG << "B+ particle parameters : "
                                << "  d0 =" << vL2Kmumu->m_getMotherTrack()->a0()
                                << "  z0 = " << vL2Kmumu->m_getMotherTrack()->z0()
                                << "  phi0 = " << vL2Kmumu->m_getMotherTrack()->phi0()
                                << "  eta = " << vL2Kmumu->m_getMotherTrack()->eta()
                                << "  pT = " << vL2Kmumu->m_getMotherTrack()->pT() << endmsg;
                            }
                            p_KmumuV = m_vertexingTool->createTrigVertex(vL2Kmumu);
                            
                            if ( p_KmumuV != NULL ){
                                if ( msgLvl() <= MSG::DEBUG) {
                                    msg() << MSG::DEBUG << "REGTEST: Kmumu vertex Fit: x = " << p_KmumuV->x() << " , y = " << p_KmumuV->y() << " , z = " << p_KmumuV->z() << endmsg;
                                    msg() << MSG::DEBUG << "REGTEST: Kmumu invariant mass = " << p_KmumuV->mass() << endmsg;
                                }
                                trigPartBmumuX->setFitmass(p_KmumuV->mass());
                                trigPartBmumuX->setFitchi2(p_KmumuV->chi2());
                                trigPartBmumuX->setFitndof(p_KmumuV->ndof());
                                trigPartBmumuX->setFitx(p_KmumuV->x());
                                trigPartBmumuX->setFity(p_KmumuV->y());
                                trigPartBmumuX->setFitz(p_KmumuV->z());
                                if ( msgLvl() <= MSG::VERBOSE) msg() << MSG::VERBOSE << "Added vertex information to B particle" << endmsg;
                                
                                m_trigBphysColl->push_back(trigPartBmumuX);
                                if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Added particle to Bphys particle collection" << endmsg;
                                
                                resultKMuMu = true;
                                PassedKMuMuVertex = true;
                                delete p_KmumuV;
                                
                            } else {
                                if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG <<"Failed to create Kmumu vertex" << endmsg;
                                delete p_KmumuV;
                                delete trigPartBmumuX;
                            }
                        } else {
                            if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG <<"Failed to create mother particle of the Kmumu L2vertex" << endmsg;
                            delete p_KmumuV;
                            delete trigPartBmumuX;
                        }
                    } else {
                        if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG <<"Failed to calculate invariant mass for the Kmumu L2vertex" << endmsg;
                        delete p_KmumuV;
                        delete trigPartBmumuX;
                    }
                } else {
                    if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG <<"Failed to fit the Kmumu L2vertex" << endmsg;
                    delete p_KmumuV;    
                    delete trigPartBmumuX;
                }
                delete vL2Kmumu;
                if ( timerSvc() ) 
                    m_VtxFitTimer->stop();
                
            } else {  //end if ( m_doVertexFit ) 
                
                m_trigBphysColl->push_back(trigPartBmumuX);
                if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "Added particle to Bphys particle collection" << endmsg;
                
            }  //end if ( m_doVertexFit ) else
            
	    } // end if to control tracks are not the same of muons	      
	  } //end for iterator over InDetTrackCollection - itrk
	} // end fot over vector InDetTrackCollection -  iterTrackColl
      } // end if ( trigBphysColl != NULL )
	// JK 22-05-08 changes for new EDM      } // end if (p_vtx !=NULL)
      
    } // end if ( (*thePair)->particleType() == TrigL2Bphys::JPSIMUMU) 
  } // end iterator over pairs TrigL2Bphys Collection
  
  if ( ( m_trigBphysColl != 0 ) && ( m_trigBphysColl->size() > 0 ) ) {
    if ( msgLvl() <= MSG::DEBUG) msg()  << MSG::DEBUG << "REGTEST: Store Bphys Collection size: " << m_trigBphysColl->size() << endmsg;
    HLT::ErrorCode sc = attachFeature(outputTE, m_trigBphysColl, "L2DiMuXFex" );
    if ( sc != HLT::OK ) {
      msg() << MSG::WARNING << "Failed to store trigBphys Collection" << endmsg;
      if ( timerSvc() ) 
	m_TotTimer->stop();        
      return sc;
    }
  } else {
    if ( msgLvl() <= MSG::DEBUG) msg() << MSG::DEBUG << "REGTEST: No Bphys Collection to store"  << endmsg;
    delete m_trigBphysColl;
  }
  m_trigBphysColl=nullptr;
 
  if ( PassedMuMuPair ) m_countPassedMuMuMassCut++;
  if ( PassedKMuMuMass ) m_countPassedKMuMuMassCut++;
  if ( PassedKMuMuVertex ) m_countPassedKMuMuVertexCut++;
  if ( resultMuMu == true ){
    if ( IdEvent != (int) m_lastEventPassedMuMu ) {
      m_countPassedEventsMuMu++;
      m_lastEventPassedMuMu=IdEvent;
    }
  }
  if ( resultKMuMu == true ) {
    if ( IdEvent != (int) m_lastEventPassedKMuMu ) {
      m_countPassedEventsKMuMu++;
      m_lastEventPassedKMuMu=IdEvent;
    }
  }
  if ( result == true ){
    m_countPassedRoIs++;
    if ( IdEvent != (int) m_lastEventPassed ) {
      m_countPassedEvents++;
      m_lastEventPassed=IdEvent;
    }
  }
  
  if ( timerSvc() ) 
    m_TotTimer->stop();    
  
  return HLT::OK;
}

